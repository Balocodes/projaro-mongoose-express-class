const mongoose = require("mongoose");

const schema = mongoose.Schema;
const current_time = new Date().toISOString();

let user_schema = new schema({
    name: { type: String, required: true },
    email: { type: String, unique: true },
    age: { type: Number },
    status: { type: Boolean, default: false },
    created_at: { type: Date, default: current_time, once: true },
    updated_at: { type: Date, default: Date.now }
});

let user_model = mongoose.model("User", user_schema);

module.exports = user_model;